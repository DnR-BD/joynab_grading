<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/22/2017
 * Time: 7:23 PM
 */

namespace App;


class Student
{
    private $name;
    private $studentId;


    public function setName($name)
    {
        $this->name = $name;
    }

    public function setStudentId($studentId)
    {
        $this->studentId = $studentId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStudentId()
    {
        return $this->studentId;
    }

}