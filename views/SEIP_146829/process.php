<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Gathering</title>


    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>


    <link rel="stylesheet" href="../../resources/style.css">



</head>

<body>



<?php
require_once("../../vendor/autoload.php");


$objStudent = new \App\Student();

$objStudent->setName($_POST["name"]);
$objStudent->setStudentId($_POST["studentID"]);





$objCourse = new \App\Course();

$objCourse->setMarkBangla($_POST["markBangla"]);
$objCourse->setGradeBangla();

$objCourse->setMarkEnglish($_POST["markEnglish"]);
$objCourse->setGradeEnglish();


$objCourse->setMarkMath($_POST["markMath"]);
$objCourse->setGradeMath();

$name = $objStudent->getName();
$studentId = $objStudent->getStudentId();

$markBangla = $objCourse->getMarkBangla();
$gradeBangla = $objCourse->getGradeBangla();

$markEnglish = $objCourse->getMarkEnglish();
$gradeEnglish = $objCourse->getGradeEnglish();


$markMath = $objCourse->getMarkMath();
$gradeMath = $objCourse->getGradeMath();


$resultPage =<<<RESULT

<div class='studentInformation'>



    <div class='form-group'>
        <label for='name'>Student's name: $name</label>
    </div>

    <div class='form-group'>
        <label for='studentID'>Student's id: $studentId </label>
    </div>

</div>
<div class='resultInformation' >

    <div class='form-group'>
        <label for='markBangla'>Bangla Mark(grade): $markBangla ($gradeBangla)</label>
    </div>

    <div class='form-group'>
        <label for='markEnglish'>english Mark(grade): $markEnglish ($gradeEnglish) </label>
    </div>
    <div class='form-group'>
        <label for='markMath'>Math Mark(grade): $markMath ($gradeMath)</label>
    </div>

</div>



RESULT;

echo $resultPage;


?>

</body>

</html>