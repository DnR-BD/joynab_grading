<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Gathering</title>


    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>


    <link rel="stylesheet" href="../../resources/style.css">



</head>
<body>

    <div class="container">

        <form action="process.php" method="post">

            <div class="studentInformation" >

            <div class="form-group">
                <label for="name">Student's name: </label>
                <input type="text" class="form-control" name="name" placeholder="enter student's name">
            </div>

                <div class="form-group">
                    <label for="studentID">Student's id: </label>
                    <input type="text" class="form-control" name="studentID" placeholder="enter student's id">
                </div>


            </div>

            <div class="resultInformation" >

                <div class="form-group">
                    <label for="markBangla">Bangla mark: </label>
                    <input type="number" step="any" min="0" max="100" class="form-control" name="markBangla" placeholder="enter bangla mark">
                </div>

                <div class="form-group">
                    <label for="markEnglish">english mark: </label>
                    <input type="number" step="any" min="0" max="100" class="form-control" name="markEnglish" placeholder="enter english mark">
                </div>
                <div class="form-group">
                    <label for="markMath">Math mark: </label>
                    <input type="number" step="any" min="0" max="100" class="form-control" name="markMath" placeholder="enter math mark">
                </div>

            </div>
            <button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>



            </form>


        </div>








</body>
</html>